/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.citygen;

import net.jevring.citygen.primitives.Building;
import net.jevring.citygen.primitives.Ground;
import net.jevring.citygen.texture.OfficeTextureGenerator;
import net.jevring.citygen.texture.ResidentialTextureGenerator;
import net.jevring.citygen.texture.TextureGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A city, full of buildings.
 *
 * @author markus@jevring.net
 */
public class City {
	private final int rows;
	private final int cols;
	private final int windowWidth;
	private final int windowHeight;
	private final int textureWidth; // in windows
	private final int textureHeight; // in windows
	private final int houseWidth;
	private final int houseDepth;
	private final int streetWidth;
	private final List<Building> buildings;
	private final OfficeTextureGenerator officeTexture;
	private final ResidentialTextureGenerator residentialTexture;
	private final TextureGenerator groundTexture;
	private final Ground ground;

	public City() {
		this(10, 10, 8, 8, 64, 64, 48, 64, 48);
	}

	public City(int rows,
	            int cols,
	            int windowWidth,
	            int windowHeight,
	            int textureWidth,
	            int textureHeight,
	            int houseWidth,
	            int houseDepth,
	            int streetWidth) {
		this.rows = rows;
		this.cols = cols;
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;
		this.textureWidth = textureWidth;
		this.textureHeight = textureHeight;
		this.houseWidth = houseWidth;
		this.houseDepth = houseDepth;
		this.streetWidth = streetWidth;

		this.officeTexture = new OfficeTextureGenerator(textureHeight, textureWidth, windowHeight, windowWidth);
		this.residentialTexture = new ResidentialTextureGenerator(textureHeight, textureWidth, windowHeight, windowWidth);
		this.groundTexture = new TextureGenerator(textureHeight, textureWidth, windowHeight, windowWidth, 0, "ground");


		// todo: have a flag that controls the hue/color temperature


		Random random = new Random();
		buildings = new ArrayList<>();
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				TextureGenerator texture;
				// todo: see if we can't accomplish some kind of "districts" here.
				// offices are likely to be closer to other offices, etc
				if (random.nextDouble() > 0.3) {
					texture = officeTexture;
				} else {
					texture = residentialTexture;
				}
				int floors = 5 + random.nextInt(100);
				buildings.add(new Building(floors,
				                           texture,
				                           houseWidth,
				                           houseDepth,
				                           row * (houseWidth + streetWidth),
				                           col * (houseDepth + streetWidth)));
			}
		}
		this.ground = new Ground(groundTexture, rows * (houseWidth + streetWidth), cols * (houseDepth + streetWidth));


	}

	public List<Building> getBuildings() {
		return buildings;
	}

	public OfficeTextureGenerator getOfficeTexture() {
		return officeTexture;
	}

	public ResidentialTextureGenerator getResidentialTexture() {
		return residentialTexture;
	}

	public TextureGenerator getGroundTexture() {
		return groundTexture;
	}

	public Ground getGround() {
		return ground;
	}
}
