/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.citygen.output;

import net.jevring.citygen.City;
import net.jevring.citygen.primitives.Building;
import net.jevring.citygen.primitives.Ground;
import net.jevring.citygen.primitives.Vertex;
import net.jevring.citygen.primitives.Wall;
import net.jevring.citygen.texture.TextureGenerator;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class that takes a city and produces output in the form of .OBJ and .MTL files.
 *
 * @author markus@jevring.net
 */
public class ObjMtlOutput implements Output {
	@Override
	public void write(City city) throws IOException {
		writeMaterialFile(city.getResidentialTexture(), 0.20);
		writeMaterialFile(city.getOfficeTexture(), 0.20);
		writeMaterialFile(city.getGroundTexture(), 0); // todo: if this needs to be 1, we need to invert things
		writeObjectFile(city.getBuildings(), city.getGround(), "city.obj");
	}

	private void writeObjectFile(List<Building> buildings, Ground ground, String filename) throws IOException {
		// without this indexing, 10x10 buildings make up 7107 lines and 212331 bytes.
		// with this indexing, 10x10 buildings make up 2415 lines and 66033 bytes.
		Map<String, Integer> vIndexes = new HashMap<>();
		Map<String, Integer> vtIndexes = new HashMap<>();
		Map<String, Integer> vnIndexes = new HashMap<>();
		List<String> vs = new ArrayList<>();
		List<String> vts = new ArrayList<>();
		List<String> vns = new ArrayList<>();
		List<String> gs = new ArrayList<>();

		for (int i = 0; i < buildings.size(); i++) {
			Building building = buildings.get(i);
			StringBuilder group = new StringBuilder();
			group.append("g Building-").append(i).append('\n');
			group.append("usemtl ").append(building.getTexture().getName()).append('\n');
			group.append("s 1\n"); // I think this is "smoothing", but I don't know what it does. I think it's been set to 1 in all obj files I've seen
			for (Wall wall : building.getWalls()) {
				// this face consists of the previous 4 added vertices, 1-indexed
				group.append("f ");
				for (Vertex vertex : wall.getVertices()) {
					Integer vIndex = addLine(vIndexes, vs, vertex.getVertexLine());
					Integer vtIndex = addLine(vtIndexes, vts, vertex.getTextureLine());
					Integer vnIndex = addLine(vnIndexes, vns, wall.getVertexNormalLine());
					group.append(vIndex).append("/").append(vtIndex).append("/").append(vnIndex).append(" ");
				}
				group.append('\n');
			}
			group.append("# 5 rectangles in the group\n\n");
			gs.add(group.toString());
		}

		StringBuilder groundGroup = new StringBuilder();
		groundGroup.append("g ground").append('\n');
		groundGroup.append("usemtl ").append("ground").append('\n');
		groundGroup.append("s 1\n"); // I think this is "smoothing", but I don't know what it does. I think it's been set to 1 in all obj files I've seen
		// this face consists of the previous 4 added vertices, 1-indexed
		groundGroup.append("f ");
		for (Vertex vertex : ground.getWall().getVertices()) {
			Integer vIndex = addLine(vIndexes, vs, vertex.getVertexLine());
			Integer vtIndex = addLine(vtIndexes, vts, vertex.getTextureLine());
			Integer vnIndex = addLine(vnIndexes, vns, ground.getWall().getVertexNormalLine());
			groundGroup.append(vIndex).append("/").append(vtIndex).append("/").append(vnIndex).append(" ");
		}
		groundGroup.append('\n');
		groundGroup.append("# 1 rectangles in the group\n\n");
		gs.add(groundGroup.toString());

		StringBuilder sb = new StringBuilder();
		sb.append("mtllib residential.mtl").append("\n");
		sb.append("mtllib office.mtl").append("\n");
		sb.append("mtllib ground.mtl").append("\n");
		sb.append("\n");
		for (String v : vs) {
			sb.append(v).append('\n');
		}
		sb.append("# ").append(vs.size()).append(" vertices\n\n");
		for (String vt : vts) {
			sb.append(vt).append('\n');
		}
		sb.append("# ").append(vts.size()).append(" texture coordinates\n\n");
		for (String vn : vns) {
			sb.append(vn).append('\n');
		}
		sb.append("# ").append(vns.size()).append(" normals\n\n");
		for (String g : gs) {
			sb.append(g).append('\n');
		}

		Files.write(Paths.get(filename), sb.toString().getBytes());
	}

	private Integer addLine(Map<String, Integer> indexes, List<String> lines, String line) {
		Integer vIndex = indexes.get(line);
		if (vIndex == null) {
			vIndex = lines.size() + 1;
			lines.add(line);
			indexes.put(line, vIndex);
		}
		return vIndex;
	}

	private void writeMaterialFile(TextureGenerator texture, double occupancy) throws IOException {
		ImageIO.write(texture.generate(occupancy), "png", new File(texture.getName() + ".png"));
		writeMaterialFile(texture);
	}

	private void writeMaterialFile(TextureGenerator texture) throws IOException {
		String materialName = texture.getName();
		String imageName = materialName + ".png";
		// by default, MTL does NOT use clamp, so we don't need to worry.
		// todo: do we need to modify this template in any way?

		// todo: do we include the hue in here?
		String template = "newmtl %s\n" +
				"illum 1\n" +
				"Kd 0.000 0.000 0.000\n" +
				"Ka 0.500 0.500 0.500\n" +
				"Ks 0.000 0.000 0.000\n" +
				"Ns 1.0\n" +
				"map_Kd %s\n";
		Files.write(Paths.get(materialName + ".mtl"), String.format(template, materialName, imageName).getBytes());
	}
}
