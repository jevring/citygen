/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.citygen.primitives;

import net.jevring.citygen.texture.TextureGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * A building in the city, with windows.
 *
 * @author markus@jevring.net
 */
public class Building {
	private final Wall front;
	private final Wall back;
	private final Wall left;
	private final Wall right;
	private final Wall roof;
	private final TextureGenerator texture;

	public Building(int floors,
	                TextureGenerator texture,
	                double houseWidth,
	                double houseDepth,
	                double locationX,
	                double locationZ) {
		this.texture = texture;

		// winding is clockwise. It's just the skybox that is inverted!
		front = new Wall(new Vertex(1, 0, 0, 0, 0),
		                 new Vertex(0, 0, 0, 1, 0),
		                 new Vertex(0, 1, 0, 1, 1),
		                 new Vertex(1, 1, 0, 0, 1),
		                 texture,
		                 Wall.NormalAxis.Z,
		                 -1);

		back = new Wall(new Vertex(0, 0, 1, 0, 0),
		                new Vertex(1, 0, 1, 1, 0),
		                new Vertex(1, 1, 1, 1, 1),
		                new Vertex(0, 1, 1, 0, 1),
		                texture,
		                Wall.NormalAxis.Z,
		                1);

		left = new Wall(new Vertex(0, 0, 0, 0, 0),
		                new Vertex(0, 0, 1, 1, 0),
		                new Vertex(0, 1, 1, 1, 1),
		                new Vertex(0, 1, 0, 0, 1),
		                texture,
		                Wall.NormalAxis.X,
		                -1);

		right = new Wall(new Vertex(1, 0, 1, 0, 0),
		                 new Vertex(1, 0, 0, 1, 0),
		                 new Vertex(1, 1, 0, 1, 1),
		                 new Vertex(1, 1, 1, 0, 1),
		                 texture,
		                 Wall.NormalAxis.X,
		                 1);

		roof = new Wall(new Vertex(1, 1, 0, 0, 0),
		                new Vertex(0, 1, 0, 0, 0),
		                new Vertex(0, 1, 1, 0, 0),
		                new Vertex(1, 1, 1, 0, 0),
		                texture,
		                Wall.NormalAxis.Y,
		                1);

		// scale the building so that it is the correct size
		int floorHeight = floors * texture.getWindowHeight();
		front.multiply(houseWidth, floorHeight, houseDepth);
		back.multiply(houseWidth, floorHeight, houseDepth);
		left.multiply(houseWidth, floorHeight, houseDepth);
		right.multiply(houseWidth, floorHeight, houseDepth);
		roof.multiply(houseWidth, floorHeight, houseDepth);

		// move the building so that it is in the correct location
		front.add(locationX, 0, locationZ);
		back.add(locationX, 0, locationZ);
		right.add(locationX, 0, locationZ);
		left.add(locationX, 0, locationZ);
		roof.add(locationX, 0, locationZ);


		// shift the texture by a random by controlled amount.
		// the shift needs to land on the items between the windows
		double windowsPerFloorInTexture = texture.getWidth() / texture.getWindowWidth();
		double horizontalShift = 1 / windowsPerFloorInTexture;

		double textureFloors = texture.getHeight() / texture.getWindowHeight();
		double verticalShift = 1 / textureFloors;

		// align the right U coordinates so that they point to something divisible with the number of windows.
		// we have to use different divisors on the sizes, as we want the windows to be the same size on all sides,
		// rather than have the same number of windows on all sides
		double windowsInWidth = houseWidth / (double) texture.getWindowWidth();
		double windowsInDepth = houseDepth / (double) texture.getWindowWidth();

		// front & back are affected by width
		front.adjustUCoordinateToWidth((1 / windowsPerFloorInTexture) * windowsInWidth);
		back.adjustUCoordinateToWidth((1 / windowsPerFloorInTexture) * windowsInWidth);

		// left & right are affected by depth
		left.adjustUCoordinateToWidth((1 / windowsPerFloorInTexture) * windowsInDepth);
		right.adjustUCoordinateToWidth((1 / windowsPerFloorInTexture) * windowsInDepth);

		Random random = new Random();
		for (Wall wall : Arrays.asList(front, back, right, left)) {
			wall.adjustVCoordinateToHeight(floors, textureFloors);
			wall.shiftTexture(random.nextInt((int) windowsPerFloorInTexture) * horizontalShift, random.nextInt((int) textureFloors) * verticalShift);
		}
	}

	public List<Wall> getWalls() {
		return Arrays.asList(front, back, left, right, roof);
	}

	public TextureGenerator getTexture() {
		return texture;
	}

	@Override
	public String toString() {
		return "Building{" +
				"front=" + front +
				", back=" + back +
				", left=" + left +
				", right=" + right +
				", roof=" + roof +
				'}';
	}
}

