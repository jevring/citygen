/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.citygen.primitives;

import net.jevring.citygen.texture.TextureGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * The ground on which the houses stand.
 *
 * @author markus@jevring.net
 */
public class Wall {
	public enum NormalAxis {
		X, Y, Z
	}

	private final Vertex bottomLeft;
	private final Vertex bottomRight;
	private final Vertex topRight;
	private final Vertex topLeft;
	private final TextureGenerator texture;
	private final NormalAxis normalAxis;
	private final double direction;

	public Wall(Vertex bottomLeft,
	            Vertex bottomRight,
	            Vertex topRight,
	            Vertex topLeft,
	            TextureGenerator texture,
	            NormalAxis normalAxis,
	            double direction) {
		this.bottomLeft = bottomLeft;
		this.bottomRight = bottomRight;
		this.topRight = topRight;
		this.topLeft = topLeft;
		this.texture = texture;
		this.normalAxis = normalAxis;
		this.direction = direction;
	}

	public String getVertexNormalLine() {
		switch (normalAxis) {
			case X:
				return String.format(Locale.US, "vn %f %f %f", direction, 0d, 0d);
			case Y:
				return String.format(Locale.US, "vn %f %f %f", 0d, direction, 0d);
			case Z:
				return String.format(Locale.US, "vn %f %f %f", 0d, 0d, direction);
			default:
				throw new AssertionError("No such normal axis: " + normalAxis);
		}

	}

	public void multiply(double x, double y, double z) {
		// MULTIPLY the vertices' coordinates with these numbers to get proper size walls

		bottomLeft.multiply(x, y, z);
		bottomRight.multiply(x, y, z);
		topRight.multiply(x, y, z);
		topLeft.multiply(x, y, z);

	}

	public void add(double x, double y, double z) {
		bottomLeft.add(x, y, z);
		bottomRight.add(x, y, z);
		topRight.add(x, y, z);
		topLeft.add(x, y, z);
		// ADD these coordinates to the vertices' coordinates

	}

	/**
	 * Ensures that the highest V coordinate depends on the number of floors.
	 * If we don't, small houses get vertically compressed textures.
	 * 
	 * @param floorsInHouse the number of floors in the building.
	 */
	public void adjustVCoordinateToHeight(double floorsInHouse, double floorsInTexture) {
		double scale = floorsInHouse / floorsInTexture;
		topRight.setVerticalScale(scale);
		topLeft.setVerticalScale(scale);
	}

	public void adjustUCoordinateToWidth(double windowsInWidth) {
		topRight.adjustUCoordinateToWidth(windowsInWidth); 
		bottomRight.adjustUCoordinateToWidth(windowsInWidth); 
	}

	public void shiftTexture(double horizontalShift, double verticalShift) {
		bottomLeft.shiftTexture(horizontalShift, verticalShift);
		bottomRight.shiftTexture(horizontalShift, verticalShift);
		topRight.shiftTexture(horizontalShift, verticalShift);
		topLeft.shiftTexture(horizontalShift, verticalShift);
	}

	public List<Vertex> getVertices() {
		return Arrays.asList(bottomLeft, bottomRight, topRight, topLeft);
	}

	@Override
	public String toString() {
		return "Wall{" +
				"bottomLeft=" + bottomLeft +
				", bottomRight=" + bottomRight +
				", topRight=" + topRight +
				", topLeft=" + topLeft +
				", texture=" + texture +
				'}';
	}
}
