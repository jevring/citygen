/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.citygen.texture;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.Arrays;
import java.util.Random;

/**
 * Generates building textures full of windows.
 * Inspired by: <a href="http://www.shamusyoung.com/twentysidedtale/?p=2954">
 * http://www.shamusyoung.com/twentysidedtale/?p=2954</a>
 *
 * @author markus@jevring.net
 */
public class TextureGenerator {
	// It feels pretty bad to crib damn near everything from that link, but man that stuff is genius!
	// 
	private final int height;
	private final int width;
	private final int windowHeight;
	private final int windowWidth;
	private final double chanceOfConsecutiveWindows;
	private final String name;

	public TextureGenerator(int heightInWindows,
	                        int widthInWindows,
	                        int windowHeight,
	                        int windowWidth,
	                        double chanceOfConsecutiveWindows,
	                        String name) {
		this.name = name;
		this.height = heightInWindows * windowHeight;
		this.width = widthInWindows * windowWidth;
		this.windowHeight = windowHeight;
		this.windowWidth = windowWidth;
		this.chanceOfConsecutiveWindows = chanceOfConsecutiveWindows;
	}

	public RenderedImage generate(double occupancy) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = image.createGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		Random random = new Random();

		for (int h = 0; h < height; h += windowHeight) {
			Color c = Color.BLACK;
			for (int w = 0; w < width; w += windowWidth) {
				// todo: include some greys
				if (random.nextDouble() > chanceOfConsecutiveWindows) { // only if we roll higher than 2/3 should we potentially change the color
					if (random.nextDouble() < occupancy) {
						c = Color.WHITE;
					} else {
						c = Color.BLACK;
					}
				}
				int[] coloredPixels = new int[(windowWidth - 1) * (windowHeight - 1)];
				Arrays.fill(coloredPixels, c.getRGB());
				image.setRGB(w + 1, h + 1, windowWidth - 1, windowHeight - 1, coloredPixels, 0, windowWidth - 1);
			}
		}
		return image;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public int getWindowHeight() {
		return windowHeight;
	}

	public int getWindowWidth() {
		return windowWidth;
	}

	public String getName() {
		return name;
	}
}
